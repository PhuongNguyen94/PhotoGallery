//
//  ViewController.swift
//  PhotoGallery
//
//  Created by Administrator on 1/23/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import SwiftPhotoGallery


class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,SwiftPhotoGalleryDelegate,SwiftPhotoGalleryDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    

    
    var ImageArray = ["image1","image2","image3"]
    
    var PickedImage = [UIImage]()
   

    @IBOutlet weak var GalleryCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GalleryCollectionView.delegate = self
        GalleryCollectionView.dataSource = self
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ImageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = GalleryCollectionView.dequeueReusableCell(withReuseIdentifier: "Cellid", for: indexPath) as! PhotoCell
        
        cell.ImageView.image = UIImage(named:ImageArray[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let gallery = SwiftPhotoGallery(delegate: self, dataSource: self )
        gallery.backgroundColor = UIColor.black
        gallery.pageIndicatorTintColor = UIColor.gray.withAlphaComponent(0.5)
        gallery.currentPageIndicatorTintColor = UIColor.white
        gallery.hidePageControl = false
        present(gallery, animated: true, completion: nil)

    }
    
    @IBAction func SaveImageButton(_ sender: Any) {
        
        print("saved")
    }
    
    
    @IBAction func CapturePhotoButton(_ sender: Any) {
        
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    //MARK : - Image Picker Controller Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedimage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            PickedImage.append(pickedimage)
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK : - Swift Photo Gallery delegate
    func numberOfImagesInGallery(gallery: SwiftPhotoGallery) -> Int {
        return ImageArray.count
    }
    
    func imageInGallery(gallery: SwiftPhotoGallery, forIndex: Int) -> UIImage? {
        return UIImage(named:ImageArray[forIndex])
    }
    
    
    func galleryDidTapToClose(gallery: SwiftPhotoGallery) {
        dismiss(animated: true, completion: nil)
    }

}




